package ru.dyatchina.tm.controller;

import java.util.List;

import static ru.dyatchina.tm.constant.Commands.*;

public class SystemController extends AbstractController {
    private final List<AbstractController> childControllers;

    public SystemController(List<AbstractController> childControllers) {
        this.childControllers = childControllers;
    }

    @Override
    public void printAcceptingCommands() {
        System.out.println(VERSION + " - display version info");
        System.out.println(HELP + " - display list of terminal commands");
        System.out.println(ABOUT + " - display developer info");
        System.out.println(EXIT + " - exit application");
    }

    @Override
    public boolean execute(String[] commandWithArgs) {
        switch (commandWithArgs[0]) {
            case "":
                return true;
            case VERSION:
                displayVersion();
                return true;
            case ABOUT:
                displayAbout();
                return true;
            case HELP:
                displayHelp();
                return true;
            case EXIT:
                System.exit(0);
            default:
                for(AbstractController controller : childControllers) {
                    if (controller.execute(commandWithArgs)) {
                        return true;
                    }
                }
                displayError();
                return false;
        }
    }

    public int displayExit() {
        System.out.println("Terminal program...");
        return 0;
    }

    public void displayError() {
        System.out.println("Error! Unknown program argument...");
    }

    public void displayWelcome() {
        System.out.println("** Welcome to task manager **");
    }

    public void displayHelp() {
        printAcceptingCommands();
        for (AbstractController controller : childControllers) {
            controller.printAcceptingCommands();
        }
    }

    public void displayAbout() {
        System.out.println("Anastasiya Dyatchina ");
        System.out.println("thenochnaya@mail.ru");
    }

    public void displayVersion() {
        System.out.println("1.0.0");
    }

}
