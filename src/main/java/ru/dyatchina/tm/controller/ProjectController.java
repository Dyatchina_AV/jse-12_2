package ru.dyatchina.tm.controller;

import ru.dyatchina.tm.entity.Project;
import ru.dyatchina.tm.service.ProjectService;

import java.util.Collections;
import java.util.List;

import static ru.dyatchina.tm.constant.Commands.*;

public class ProjectController  extends AbstractController {

    private final ProjectService projectService;

    public ProjectController(ProjectService projectService) {

        this.projectService = projectService;
    }

    @Override
    public void printAcceptingCommands() {
        System.out.println(PROJECT_CREATE + " <name> <description> - create new Project");
        System.out.println(PROJECT_LIST + " list all Projects");
        System.out.println(PROJECT_CLEAR + " clear all Projects");
        System.out.println(PROJECT_FIND_BY_ID + " <id> find Project by id");
        System.out.println(PROJECT_FIND_BY_INDEX + " <index> find Project by index");
        System.out.println(PROJECT_FIND_BY_NAME + " <name> find Project by name");
        System.out.println(PROJECT_DELETE_BY_ID + " <id> delete Project by id");
        System.out.println(PROJECT_DELETE_BY_INDEX + " <index> delete Project by index");
        System.out.println(PROJECT_DELETE_BY_NAME + " <name> delete Project by name");
    }

    @Override
    public boolean execute(String[] commandWithArgs) {
        switch (commandWithArgs[0]) {
            case PROJECT_FIND_BY_ID:
                findProjectById(commandWithArgs);
                return true;
            case PROJECT_FIND_BY_INDEX:
                findProjectByIndex(commandWithArgs);
                return true;
            case PROJECT_FIND_BY_NAME:
                findProjectByName(commandWithArgs);
                return true;
            case PROJECT_DELETE_BY_ID:
                deleteProjectById(commandWithArgs);
                return true;
            case PROJECT_DELETE_BY_INDEX:
                deleteProjectByIndex(commandWithArgs);
                return true;
            case PROJECT_DELETE_BY_NAME:
                deleteProjectByName(commandWithArgs);
                return true;
            case PROJECT_CREATE:
                createProject(commandWithArgs);
                return true;
            case PROJECT_LIST:
                printProjects(projectService.getAllProjects());
                return true;
            case PROJECT_CLEAR:
                clearProjects();
                return true;
            default:
                return false;
        }
    }

    private void deleteProjectById(String[] commandWithArgs) {
        if (commandWithArgs.length < 2) {
            System.out.println("Error! No argument");
            return;
        }
        try {
            Long id = Long.parseLong(commandWithArgs[1]);
            Project deleted = projectService.deleteProjectById(id);
            if (deleted != null) {
                System.out.println("Project deleted");
            }
        } catch (NumberFormatException e) {
            System.out.println("Error! Invalid argument");
        }
    }

    private void deleteProjectByIndex(String[] commandWithArgs) {
        if (commandWithArgs.length < 2) {
            System.out.println("Error! No argument");
            return;
        }
        try {
            Integer index = Integer.parseInt(commandWithArgs[1]);
            Project deleted = projectService.deleteProjectByIndex(index);
            if (deleted != null) {
                System.out.println("Project deleted");
            }
        } catch (NumberFormatException e) {
            System.out.println("Error! Invalid argument");
        }
    }

    private void deleteProjectByName(String[] commandWithArgs) {
        if (commandWithArgs.length < 2) {
            System.out.println("Error! No argument");
            return;
        }
        List<Project> deleted = projectService.deleteProjectByName(commandWithArgs[1]);
        if (!deleted.isEmpty()) {
            System.out.println("Projects deleted");
        }
    }

    private void findProjectById(String[] commandWithArgs) {
        if (commandWithArgs.length < 2) {
            System.out.println("Error! No argument");
            return;
        }
        try {
            Long id = Long.parseLong(commandWithArgs[1]);
            Project project = projectService.getProjectById(id);
            printProject(project);
        } catch (NumberFormatException e) {
            System.out.println("Error! Invalid argument");
        }
    }

    private void findProjectByIndex(String[] commandWithArgs) {
        if (commandWithArgs.length < 2) {
            System.out.println("Error! No argument");
            return;
        }
        try {
            Integer index = Integer.parseInt(commandWithArgs[1]);
            Project project = projectService.getProjectByIndex(index);
            printProject(project);
        } catch (NumberFormatException e) {
            System.out.println("Error! Invalid argument");
        }
    }

    private void findProjectByName(String[] commandWithArgs) {
        if (commandWithArgs.length < 2) {
            System.out.println("Error! No argument");
            return;
        }
        List<Project> projects = (List<Project>) projectService.getProjectByName(commandWithArgs[1]);
        printProjects(projects);
    }

    private void createProject(String[] args) {
        projectService.create(
                args.length > 1 ? args[1] : null,
                args.length > 2 ? args[2] : null);
        System.out.println("Project created");
    }

    private void printProject(Project project) {
        if (project != null) {
            printProjects(Collections.singletonList(project));
        }
    }

    private void printProjects(List<Project> projects) {
        String[][] printData = new String[projects.size() + 1][3];
        printData[0][0] = " ID ";
        printData[0][1] = " Name ";
        printData[0][2] = " Description ";

        int row = 1;
        for (Project project : projects) {
            printData[row][0] = String.valueOf(project.getId());
            printData[row][1] = String.valueOf(project.getName());
            printData[row][2] = String.valueOf(project.getDescription());
            row++;
        }

        printDataWithHeader(printData);
    }

    private void clearProjects() {
        projectService.deleteAllProjects();
        System.out.println("All Projects deleted");
    }
}
