package ru.dyatchina.tm.controller;

import ru.dyatchina.tm.entity.User;
import ru.dyatchina.tm.enums.UserRole;
import ru.dyatchina.tm.service.UserService;

import java.util.List;

import static ru.dyatchina.tm.constant.Commands.*;

public class UserController extends AbstractController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void printAcceptingCommands() {
        System.out.println(USER_CREATE + " <login> <password> <role> - create User, roles: USER|ADMIN");
        System.out.println(USER_LIST + " list all Users");
        System.out.println(USER_CLEAR + " delete all Users");
    }

    @Override
    public boolean execute(String[] commandWithArgs) {
        switch (commandWithArgs[0]) {
            case USER_CREATE:
                createUser(commandWithArgs);
                return true;
            case USER_CLEAR:
                userService.deleteAllUsers();
                System.out.println("All users deleted");
                return true;
            case USER_LIST:
                printUsers(userService.getAllUsers());
                return true;
            default:
                return false;
        }
    }

    private void createUser(String[] commandWithArgs) {
        if (commandWithArgs.length < 4) {
            System.out.println("Error! No argument");
        }
        try {
            String login = commandWithArgs[1];
            String password = commandWithArgs[2];
            UserRole role = UserRole.valueOf(commandWithArgs[3]);
            boolean created = userService.createUser(login, password, role);
            if (created) {
                System.out.println("User created");
            } else {
                System.out.println("User not created");
            }
        } catch (IllegalArgumentException e) {
            System.out.println("Error! Invalid argument");
        }
    }

    private void printUsers(List<User> users) {
        String[][] printData = new String[users.size() + 1][3];
        printData[0][0] = " ID ";
        printData[0][1] = " Login ";
        printData[0][2] = " Role ";

        int row = 1;
        for (User user : users) {
            printData[row][0] = String.valueOf(user.getId());
            printData[row][1] = String.valueOf(user.getLogin());
            printData[row][2] = String.valueOf(user.getRole());
            row++;
        }

        printDataWithHeader(printData);
    }
}

