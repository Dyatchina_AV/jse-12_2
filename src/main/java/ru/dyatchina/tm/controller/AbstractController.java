package ru.dyatchina.tm.controller;

import java.util.Scanner;

public abstract class AbstractController {

    public abstract void printAcceptingCommands();

    public abstract boolean execute(String[] commandWithArgs);

    protected void printDataWithHeader(String[][] data) {
        int[] columnsMaxWidth = new int[data[0].length];

        for (int row = 0; row < data.length; row++) {
            for (int column = 0; column < data[0].length; column++) {
                columnsMaxWidth[column] = Integer.max(columnsMaxWidth[column], data[row][column].length());
            }
        }

        for (int row = 0; row < data.length; row++) {
            for (int column = 0; column < data[0].length; column++) {
                System.out.print("|");
                System.out.print(data[row][column]);
                printSpaces(columnsMaxWidth[column] - data[row][column].length());
            }
            System.out.print("|");
            System.out.println();
        }
    }

    protected void printSpaces(int count) {
        for (int i = 0; i < count; i++) {
            System.out.print(" ");
        }
    }

    protected final Scanner scanner = new Scanner(System.in);

}
