package ru.dyatchina.tm.controller;

import ru.dyatchina.tm.entity.Task;
import ru.dyatchina.tm.service.TaskService;

import java.util.Collections;
import java.util.List;

import static ru.dyatchina.tm.constant.Commands.*;

public class TaskController extends AbstractController {

    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void printAcceptingCommands() {
        System.out.println(TASK_CREATE + "<name> <description> - create new Task");
        System.out.println(TASK_CLEAR + "list all Tasks");
        System.out.println(TASK_LIST + "clear all Tasks");
        System.out.println(TASK_FIND_BY_ID + "<id> find task by id");
        System.out.println(TASK_FIND_BY_INDEX + "<index> find task by index");
        System.out.println(TASK_FIND_BY_NAME + "<name> find task by name");
        System.out.println(TASK_DELETE_BY_ID + "<id> delete task by id");
        System.out.println(TASK_DELETE_BY_INDEX + "<index> delete task by index");
        System.out.println(TASK_DELETE_BY_NAME + "<name> delete task by name");
        System.out.println(TASK_LIST_BY_PROJECT + "<project id> list all tasks in project");
        System.out.println(TASK_ADD_TO_PROJECT + "<project id> <task id> add task to project");
        System.out.println(TASK_DELETE_FROM_PROJECT + "<task id> delete task from project");
    }

    @Override
    public boolean execute(String[] commandWithArgs) {
        switch (commandWithArgs[0]) {
            case TASK_CREATE:
                createTask(commandWithArgs);
                return true;
            case TASK_LIST:
                printTasks(taskService.getAllTask());
                return true;
            case TASK_CLEAR:
                clearTasks();
                return true;
            case TASK_FIND_BY_ID:
                findTaskById(commandWithArgs);
                return true;
            case TASK_FIND_BY_INDEX:
                findTaskByIndex(commandWithArgs);
                return true;
            case TASK_FIND_BY_NAME:
                findTaskByName(commandWithArgs);
                return true;
            case TASK_DELETE_BY_ID:
                deleteTaskById(commandWithArgs);
                return true;
            case TASK_DELETE_BY_INDEX:
                deleteTaskByIndex(commandWithArgs);
                return true;
            case TASK_DELETE_BY_NAME:
                deleteTaskByName(commandWithArgs);
                return true;
            case TASK_LIST_BY_PROJECT:
                listTaskByProjectId(commandWithArgs);
                return true;
            case TASK_ADD_TO_PROJECT:
                addTaskToProject(commandWithArgs);
                return true;
            case TASK_DELETE_FROM_PROJECT:
                deleteTaskFromProject(commandWithArgs);
                return true;
            default:
                return false;
        }
    }

    private void deleteTaskFromProject(String[] commandWithArgs) {
        if (commandWithArgs.length < 2) {
            System.out.println("Error! No argument");
            return;
        }
        try {
            Long taskId = Long.parseLong(commandWithArgs[1]);
            boolean success = taskService.deleteTaskFromProject(taskId);
            if (success) {
                System.out.println("Task deleted from Project");
            } else {
                System.out.println("Task not deleted from Project");
            }
        } catch (NumberFormatException e) {
            System.out.println("Error! Invalid argument");
        }
    }

    private void addTaskToProject(String[] commandWithArgs) {
        if (commandWithArgs.length < 3) {
            System.out.println("Error! No argument");
            return;
        }
        try {
            Long projectId = Long.parseLong(commandWithArgs[1]);
            Long taskId = Long.parseLong(commandWithArgs[2]);
            boolean success = taskService.addTaskToProject(projectId, taskId);
            if (success) {
                System.out.println("Task added to Project");
            } else {
                System.out.println("Task not added to Project");
            }
        } catch (NumberFormatException e) {
            System.out.println("Error! Invalid argument");
        }
    }

    private void listTaskByProjectId(String[] commandWithArgs) {
        if (commandWithArgs.length < 2) {
            System.out.println("Error! No argument");
            return;
        }
        try {
            Long projectId = Long.parseLong(commandWithArgs[1]);
            List<Task> tasks = taskService.getTaskByProject(projectId);
            printTasks(tasks);
        } catch (NumberFormatException e) {
            System.out.println("Error! Invalid argument");
        }
    }

    private void deleteTaskById(String[] commandWithArgs) {
        if (commandWithArgs.length < 2) {
            System.out.println("Error! No argument");
            return;
        }
        try {
            Long id = Long.parseLong(commandWithArgs[1]);
            Task deleted = taskService.deleteTaskById(id);
            if (deleted != null) {
                System.out.println("Task deleted");
            }
        } catch (NumberFormatException e) {
            System.out.println("Error! Invalid argument");
        }
    }

    private void deleteTaskByIndex(String[] commandWithArgs) {
        if (commandWithArgs.length < 2) {
            System.out.println("Error! No argument");
            return;
        }
        try {
            Integer index = Integer.parseInt(commandWithArgs[1]);
            Task deleted = taskService.deleteTaskByIndex(index);
            if (deleted != null) {
                System.out.println("Task deleted");
            }
        } catch (NumberFormatException e) {
            System.out.println("Error! Invalid argument");
        }
    }

    private void deleteTaskByName(String[] commandWithArgs) {
        if (commandWithArgs.length < 2) {
            System.out.println("Error! No argument");
            return;
        }
        List<Task> deleted = taskService.deleteTaskByName(commandWithArgs[1]);
        if (!deleted.isEmpty()) {
            System.out.println("Tasks deleted");
        }
    }

    private void findTaskById(String[] commandWithArgs) {
        if (commandWithArgs.length < 2) {
            System.out.println("Error! No argument");
            return;
        }
        try {
            Long id = Long.parseLong(commandWithArgs[1]);
            Task task = taskService.getTaskById(id);
            printTask(task);
        } catch (NumberFormatException e) {
            System.out.println("Error! Invalid argument");
        }
    }

    private void findTaskByIndex(String[] commandWithArgs) {
        if (commandWithArgs.length < 2) {
            System.out.println("Error! No argument");
            return;
        }
        try {
            Integer index = Integer.parseInt(commandWithArgs[1]);
            Task task = taskService.getTaskByIndex(index);
            printTask(task);
        } catch (NumberFormatException e) {
            System.out.println("Error! Invalid argument");
        }
    }

    private void findTaskByName(String[] commandWithArgs) {
        if (commandWithArgs.length < 2) {
            System.out.println("Error! No argument");
            return;
        }
        List<Task> tasks = taskService.getTaskByName(commandWithArgs[1]);
        printTasks(tasks);
    }

    private void clearTasks() {
        taskService.deleteAllTask();
        System.out.println("All Tasks deleted");
    }

    private void createTask(String[] args) {
        taskService.create(
                args.length > 1 ? args[1] : null,
                args.length > 2 ? args[2] : null);
        System.out.println("Task created");
    }

    private void printTask(Task task) {
        if (task != null) {
            printTasks(Collections.singletonList(task));
        }
    }

    private void printTasks(List<Task> tasks) {
        String[][] printData = new String[tasks.size() + 1][4];
        printData[0][0] = " ID ";
        printData[0][1] = " Project ID ";
        printData[0][2] = " Name ";
        printData[0][3] = " Description ";

        int row = 1;
        for (Task task : tasks) {
            printData[row][0] = String.valueOf(task.getId());
            printData[row][1] = String.valueOf(task.getProjectId());
            printData[row][2] = String.valueOf(task.getName());
            printData[row][3] = String.valueOf(task.getDescription());
            row++;
        }

        printDataWithHeader(printData);
    }
}
