package ru.dyatchina.tm.enums;

public enum UserRole {
    ADMIN, USER;
}
