package ru.dyatchina.tm.repository;

import ru.dyatchina.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

public class TaskRepository {

    private final List<Task> tasks = new ArrayList<>();
    private final AtomicLong idGenerator = new AtomicLong();

    public void save(Task task) {
        task.setId(idGenerator.incrementAndGet());
        tasks.add(task);
    }

    public List<Task> findAll() {
        return tasks;
    }

    public Task findById(Long id) {
        if (id == null) return null;
        for (Task task: tasks) {
            if (task.getId().equals(id))
                return task;
        }
        return null;
    }

    public Task findByIndex(int index) {
        if (index < 0 || index > tasks.size() - 1) return null;
        return tasks.get(index);
    }

    public List<Task> findByName(String name) {
        List<Task> result = new ArrayList<>();
        for (Task task: tasks) {
            if (Objects.equals(task.getName(), name)) {
                result.add(task);}
        }
        return result;
    }

    public List<Task> findAddByProjectId(Long projectId) {
        List<Task> result = new ArrayList<>();
        for (Task task: tasks) {
            if (Objects.equals(task.getProjectId(), projectId)) {
                result.add(task);
            }
        }
        return result;
    }

    public void deleteAll() {
        tasks.clear();
    }

    public Task deleteById(Long id) {
        Task task = findById(id);
        if (task != null) {
            tasks.remove(task);
        }
        return task;
    }

    public Task deleteByIndex(int index) {
        if (tasks.size() > index) {
            return tasks.remove(index);
        }
        return null;
    }

    public List<Task> deleteByName(String name) {
        List<Task> toRemove = findByName(name);
        tasks.removeAll(toRemove);
        return toRemove;
    }

}
