package ru.dyatchina.tm.repository;

import ru.dyatchina.tm.entity.Project;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

public class ProjectRepository{

    private final List<Project> projects = new ArrayList<>();
    private final AtomicLong idGenerator = new AtomicLong();

    public void save(Project project) {
        project.setId(idGenerator.incrementAndGet());
        projects.add(project);
    }

    public List<Project> findAll() {
        return projects;
    }

    public Project findById(Long id) {
        if (id == null) return null;
        for (Project project: projects) {
            if (project.getId().equals(id)) return project;
        }
        return null;
    }

    public Project findByIndex(int index) {
        if (projects.size() > index) {
            return projects.get(index);
        }
        return null;
    }

    public Project findByName(String name) {
        List<Project> result = new ArrayList<>();
        if (name == null || name.isEmpty() ) return null;
        for (final Project project: projects) {
            if (Objects.equals(project.getName(), name)) {
                result.add(project);
            }
        }
        return null;
    }

    public void deleteAll() {
        projects.clear();
    }

    public Project deleteById(Long id) {
        Project project = findById(id);
        if (project != null) {
            projects.remove(project);
        }
        return project;
    }

    public Project deleteByIndex(int index) {
        if (projects.size() > index) {
            return projects.remove(index);
        }
        return null;
    }

    public List<Project> deleteByName(String name) {
        List<Project> toRemove = (List<Project>) findByName(name);
        projects.removeAll(toRemove);
        return toRemove;
    }

}