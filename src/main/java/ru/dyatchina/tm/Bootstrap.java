package ru.dyatchina.tm;

import ru.dyatchina.tm.controller.*;
import ru.dyatchina.tm.repository.ProjectRepository;
import ru.dyatchina.tm.repository.TaskRepository;
import ru.dyatchina.tm.repository.UserRepository;
import ru.dyatchina.tm.service.AuthService;
import ru.dyatchina.tm.service.ProjectService;
import ru.dyatchina.tm.service.TaskService;
import ru.dyatchina.tm.service.UserService;

import java.util.Arrays;
import java.util.Scanner;

public class Bootstrap {

    private final ProjectRepository projectRepository = new ProjectRepository();
    private final TaskRepository taskRepository = new TaskRepository();
    private final UserRepository userRepository = new UserRepository();

    private final UserService userService = new UserService(userRepository);
    private final AuthService authService = new AuthService(userService);
    private final ProjectService projectService = new ProjectService(projectRepository);
    private final TaskService taskService = new TaskService(taskRepository, projectRepository, userRepository,
            authService);

    private final ProjectController projectController = new ProjectController(projectService);
    private final TaskController taskController = new TaskController(taskService);
    private final UserController userController = new UserController(userService);
    private final AuthController authController = new AuthController(authService);
    private final SystemController systemController = new SystemController(
            Arrays.asList(projectController, taskController, userController, authController));

    public void start() {
        displayWelcome();

        Scanner consoleScanner = new Scanner(System.in);
        String command = null;
        while((command = consoleScanner.nextLine()) != null) {
            run(command);
        }
    }

    private void run(String command) {
        String[] commandWithArgs = command.split(" ");
        systemController.execute(commandWithArgs);
    }

    private void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }
}
