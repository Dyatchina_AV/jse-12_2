package ru.dyatchina.tm.service;

import ru.dyatchina.tm.entity.Project;
import ru.dyatchina.tm.repository.ProjectRepository;

import java.util.List;

public class ProjectService {

     private final ProjectRepository projectRepository;

     public ProjectService(ProjectRepository projectRepository) {

         this.projectRepository = projectRepository;
     }

     public void create(String name, String description) {
         Project project = new Project();
         project.setName(name);
         project.setDescription(description);
         projectRepository.save(project);
    }

    public List<Project> getAllProjects() {
        return projectRepository.findAll();
    }

    public Project getProjectById(Long id) {
         return projectRepository.findById(id);
    }

    public Project getProjectByIndex(int index) {
        return projectRepository.findByIndex(index);
    }

    public Project getProjectByName(String name) {
        return projectRepository.findByName(name);
    }

    public void deleteAllProjects() {

         projectRepository.deleteAll();
    }

    public Project deleteProjectById(Long id) {
        return projectRepository.deleteById(id);
    }

    public Project deleteProjectByIndex(int index) {
        return projectRepository.deleteByIndex(index);
    }

    public List<Project> deleteProjectByName(String name) {
        return projectRepository.deleteByName(name);
    }

}
