package ru.dyatchina.tm.service;

import org.apache.commons.codec.digest.DigestUtils;
import ru.dyatchina.tm.entity.User;
import ru.dyatchina.tm.enums.UserRole;

public class AuthService {
    private final UserService userService;
    private User currentUser = null;

    public AuthService(UserService userService) {
        this.userService = userService;
        userService.createUser("admin", "admin", UserRole.ADMIN);
    }

    public boolean login(String login, String password) {
        User user = userService.getUserByLogin(login);
        if (user != null && user.getPassword().equals(DigestUtils.md5Hex(password))) {
            currentUser = user;
            return true;
        }
        return false;
    }

    public void logout() {
        currentUser = null;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public boolean changeCurrentUserPassword(String currentPassword, String newPassword) {
        if (currentUser == null) {
            System.out.println("Not authenticated");
            return false;
        }
        if (!currentUser.getPassword().equals(DigestUtils.md5Hex(currentPassword))) {
            System.out.println("Incorrect password");
            return false;
        }
        currentUser.setPassword(DigestUtils.md5Hex(newPassword));
        userService.updateUser(currentUser);
        return true;
    }

    public boolean changeUserPassword(String login, String newPassword) {
        if (currentUser == null) {
            System.out.println("Not authenticated");
            return false;
        }
        if (currentUser.getRole() != UserRole.ADMIN) {
            System.out.println("Incorrect role");
            return false;
        }
        User user = userService.getUserByLogin(login);
        if (user == null) {
            System.out.println("User not found");
            return false;
        }
        user.setPassword(DigestUtils.md5Hex(newPassword));
        userService.updateUser(user);
        return true;
    }
}

